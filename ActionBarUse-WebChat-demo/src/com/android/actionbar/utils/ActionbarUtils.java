package com.android.actionbar.utils;

import java.lang.reflect.Field;

import android.content.Context;
import android.view.ViewConfiguration;

public class ActionbarUtils {

	public static void setOverflowShowingAlways(Context ctx) {
		try {
			ViewConfiguration config = ViewConfiguration.get(ctx);
			Field menuKeyField = ViewConfiguration.class
					.getDeclaredField("sHasPermanentMenuKey");
			menuKeyField.setAccessible(true);
			menuKeyField.setBoolean(config, false);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

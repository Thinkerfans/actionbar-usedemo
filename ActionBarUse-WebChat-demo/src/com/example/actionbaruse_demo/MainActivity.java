package com.example.actionbaruse_demo;

import java.lang.reflect.Method;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;

import com.android.actionbar.utils.ActionbarTabListener;
import com.android.actionbar.utils.ActionbarUtils;

public class MainActivity extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);


		ActionbarUtils.setOverflowShowingAlways(this);
		initTabs();

	}
	
	
	private void initTabs(){
		ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		Tab tab = actionBar
				.newTab()
				.setText(R.string.tab_chat)
				.setTabListener(
						new ActionbarTabListener<ChatFragment>(this, getString(R.string.tab_chat),
								ChatFragment.class));
		actionBar.addTab(tab);

		tab = actionBar
				.newTab()
				.setText(R.string.tab_find)
				.setTabListener(
						new ActionbarTabListener<FoundFragment>(this, getString(R.string.tab_find),
								FoundFragment.class));
		actionBar.addTab(tab);
		
		tab = actionBar
				.newTab()
				.setText(R.string.tab_contact)
				.setTabListener(
						new ActionbarTabListener<ContactsFragment>(this, getString(R.string.tab_contact),
								ContactsFragment.class));
		actionBar.addTab(tab);		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onMenuOpened(int featureId, Menu menu) {
		
		Log.e("onMenuOpened ","test --------"+menu);
	    

		if (featureId == Window.FEATURE_ACTION_BAR && menu != null) {
			if (menu.getClass().getSimpleName().equals("MenuBuilder")) {
				try {
					Method m = menu.getClass().getDeclaredMethod(
							"setOptionalIconsVisible", Boolean.TYPE);
					m.setAccessible(true);
					m.invoke(menu, true);
				} catch (Exception e) {
				}
			}
			
//			Log.e("onMenuOpened ","test --------"+menu);
//			 getMenuInflater().inflate(R.menu.main, menu);
//			 MenuItem searchItem = menu.findItem(R.id.action_search);
//			 SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
		}
		

	    
		return super.onMenuOpened(featureId, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		Log.e("onMenuOpened ","id --------"+item.getItemId());
	    
		switch (item.getItemId()) {
		

		case R.id.action_search:
			
	        return true;

        case R.id.action_plus:

            return true;
        case R.id.action_feed:

            return true;
        case R.id.action_settings:
        	startActivity(new Intent(this,SettingActivity.class));
            return true;
        case R.id.action_card:

            return true;
        case R.id.action_album:

            return true;
        case R.id.action_collection:

            return true;
        default:
            return super.onOptionsItemSelected(item);
		}

	}

}
